/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package mvc;

import controlador.Controlador;
import javax.swing.JFrame;
import modelo.Producto;
import vista.dlgProductos;

/**
 *
 * @author emoru
 */
public class MVC1 {
 
        public static void main(String[] args) {
        // Crear objetos del modelo, vista y controlador
        Producto pro = new Producto();
        dlgProductos vista = new dlgProductos(new JFrame(), true);
        Controlador contra = new Controlador(pro, vista);
        
        // Iniciar la vista
        contra.iniciarVista();
    }
}
    


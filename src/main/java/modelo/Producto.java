package modelo;

public class Producto {
    private String codigoP;
    private String descripcion;
    private String unidadesDeMedida;
    private float precioC;
    private float precioV;
    private int cantidad;
    
    public Producto() {
        // Constructor vacío
    }
    
    // Constructor con parámetros
    public Producto(String codigoP, String descripcion, String unidadesDeMedida, float precioC, float precioV, int cantidad) {
        this.codigoP = codigoP;
        this.descripcion = descripcion;
        this.unidadesDeMedida = unidadesDeMedida;
        this.precioC = precioC;
        this.precioV = precioV;
        this.cantidad = cantidad;
    }
    
    // Constructor de copia
    public Producto(Producto producto) {
        this.codigoP = producto.codigoP;
        this.descripcion = producto.descripcion;
        this.unidadesDeMedida = producto.unidadesDeMedida;
        this.precioC = producto.precioC;
        this.precioV = producto.precioV;
        this.cantidad = producto.cantidad;
    }
    
    // Métodos getter y setter para cada atributo
    
    public String getCodigo() {
        return this.codigoP;
    }
    
    public void setCodigo(String codigoP) {
        this.codigoP = codigoP;
    }
    
    public String getDescripcion() {
        return this.descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public String getUnidadesDeMedida() {
        return this.unidadesDeMedida;
    }
    
    public void setUnidadesDeMedida(String unidadesDeMedida) {
        this.unidadesDeMedida = unidadesDeMedida;
    }
    
    public float getPrecioC() {
        return this.precioC;
    }
    
    public void setPrecioC(float precioC) {
        this.precioC = precioC;
    }
    
    public float getPrecioV() {
        return this.precioV;
    }
    
    public void setPrecioV(float precioV) {
        this.precioV = precioV;
    }
    
    public int getCantidad() {
        return this.cantidad;
    }
    
    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
    
    // Métodos para calcular el precio de venta, el precio de compra y la ganancia
    
    public float calcularPrecioVenta() {
        return this.precioV * this.cantidad;
    }
    
    public float calcularPrecioCompra() {
        return this.precioC * this.cantidad;
    }
    
    public float calcularGanancia() {
        return this.calcularPrecioVenta() - this.calcularPrecioCompra();
    }
    
    // Métodos adicionales para obtener información
    
    public String obtenerDescripcion() {
        return this.descripcion;
    }
    
    public String obtenerUnidadesDeMedida() {
        return this.unidadesDeMedida;
    }
    
    public String obtenerCodigo() {
        return this.codigoP;
    }
}


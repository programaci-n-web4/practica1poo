package controlador;

import modelo.Producto;
import vista.dlgProductos;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Controlador implements ActionListener {
    
    private Producto producto; // Referencia al modelo Producto
    private dlgProductos vista; // Referencia a la vista dlgProductos

    public Controlador(Producto producto, dlgProductos vista) {
        this.producto = producto;
        this.vista = vista;
    
        // Configurar los botones de la vista para que el controlador los escuche
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
    }
    
    public void iniciarVista() {
        vista.setTitle(":: Productos ::");
        vista.setSize(500, 500);
        vista.setVisible(true);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vista.btnLimpiar) {
            // Limpiar los campos de entrada en la vista
            vista.txtCantidad.setText("");
            vista.txtCodigo.setText("");
            vista.txtDescripcion.setText("");
            vista.txtPCompra.setText("");
            vista.txtPVenta.setText("");
            vista.txtUnidadDeMedida.setText("");
            vista.txtTotalGanancia.setText("");
            vista.txtTotalVenta.setText("");
            vista.txtTotalCompra.setText("");
        }
        
        if (e.getSource() == vista.btnNuevo) {
            // Habilitar los campos de entrada y botones relacionados
            vista.txtCantidad.setEnabled(true); 
            vista.txtCodigo.setEnabled(true);
            vista.txtDescripcion.setEnabled(true);
            vista.txtUnidadDeMedida.setEnabled(true);
            vista.txtPCompra.setEnabled(true);
            vista.txtPVenta.setEnabled(true);
            vista.btnGuardar.setEnabled(true);
            vista.btnMostrar.setEnabled(true);
        }
        
        if (e.getSource() == vista.btnGuardar) {
            // Obtener los valores de los campos de entrada y establecerlos en el modelo Producto
            producto.setCodigo(vista.txtCodigo.getText());
            producto.setDescripcion(vista.txtDescripcion.getText());
            producto.setUnidadesDeMedida(vista.txtUnidadDeMedida.getText());
            
            try {
                // Convertir los valores de precio y cantidad a tipos numéricos
                producto.setPrecioC(Float.parseFloat(vista.txtPCompra.getText()));
                producto.setPrecioV(Float.parseFloat(vista.txtPVenta.getText()));
                producto.setCantidad(Integer.parseInt(vista.txtCantidad.getText()));
                
                JOptionPane.showMessageDialog(vista, "Se agregó exitosamente");
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: " + ex.getMessage());
            } catch (Exception ex2) {
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: " + ex2.getMessage());
            }
        }
        
        if (e.getSource() == vista.btnMostrar) {
            // Mostrar los valores del modelo Producto en los campos de la vista
            vista.txtCodigo.setText(producto.getCodigo());
            vista.txtDescripcion.setText(producto.getDescripcion());
            vista.txtUnidadDeMedida.setText(producto.getUnidadesDeMedida());
            vista.txtCantidad.setText(String.valueOf(producto.getCantidad()));        
            
            // Calcular y mostrar los totales relacionados con el producto
            vista.txtTotalCompra.setText(String.valueOf(producto.calcularPrecioCompra()));
            vista.txtTotalVenta.setText(String.valueOf(producto.calcularPrecioVenta()));
            vista.txtTotalGanancia.setText(String.valueOf(producto.calcularGanancia()));
        }
        
        if (e.getSource() == vista.btnCerrar) {
            // Mostrar un cuadro de diálogo de confirmación para cerrar la ventana
            int option = JOptionPane.showConfirmDialog(vista, "Seguro que quieres salir", "Decide", JOptionPane.YES_NO_OPTION);
            
            if (option == JOptionPane.YES_NO_OPTION) {
                vista.dispose();
                System.exit(0);
            }
        }
        
        if (e.getSource() == vista.btnCancelar) {
            // Limpiar los campos de entrada y deshabilitar los campos y botones relacionados
            vista.txtCantidad.setText("");
            vista.txtCodigo.setText("");
            vista.txtDescripcion.setText("");
            vista.txtUnidadDeMedida.setText("");
            vista.txtPCompra.setText("");
            vista.txtPVenta.setText("");
            vista.txtTotalCompra.setText("");
            vista.txtTotalGanancia.setText("");
            vista.txtTotalVenta.setText("");
            vista.txtCantidad.setEnabled(false);
            vista.txtCodigo.setEnabled(false);
            vista.txtDescripcion.setEnabled(false);
            vista.txtUnidadDeMedida.setEnabled(false);
            vista.txtPCompra.setEnabled(false);
            vista.txtPVenta.setEnabled(false);
            vista.btnMostrar.setEnabled(false);
            vista.btnGuardar.setEnabled(false);
        }
    }
    
}
